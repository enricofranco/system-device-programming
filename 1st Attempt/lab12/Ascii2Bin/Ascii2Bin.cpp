#define UNICODE
#define _UNICODE

#ifdef UNICODE
#define TCHAR WCHAR
#else
#define TCHAR CHAR
#endif

#define _CRT_SECURE_NO_WARNINGS
#include <Windows.h>
#include <tchar.h>
#include <stdio.h>

#define IP_LENGHT 16
#define USER_LENGHT 25
#define DATE_LENGHT 21
#define TIME_LENGHT 9

typedef struct _record {
	TCHAR ip[IP_LENGHT];
	TCHAR user[USER_LENGHT];
	TCHAR date[DATE_LENGHT];
	TCHAR duration[TIME_LENGHT];
} RECORD;
typedef RECORD* LPRECORD;

INT _tmain(INT argc, LPTSTR argv[]) {
	FILE* fpIn;
	HANDLE hOut;
	RECORD r;

	if (argc != 3) {
		_ftprintf(stderr, _T("Parameter errors %s file_input file_output\n"), argv[0]);
		return 1;
	}

	fpIn = _tfopen(argv[1], _T("r"));
	if (fpIn == NULL) {
		_ftprintf(stderr, _T("Cannot open input file %s. Error: %x\n"),
			argv[1], GetLastError());
		return 2;
	}

	hOut = CreateFile(argv[2], GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL, NULL);
	if (hOut == INVALID_HANDLE_VALUE) {
		_ftprintf(stderr, _T("Cannot open output file %s. Error: %x\n"),
			argv[2], GetLastError());
		fclose(fpIn);
		return 3;
	}

	while (_ftscanf(fpIn, _T("%s %s %s %s"),
		&r.ip, &r.user, &r.date, &r.duration) == 4) {
		_tprintf(_T("Line read: %s %s %s %s\n"),
			r.ip, r.user, r.date, r.duration);
		DWORD bytesWritten;
		WriteFile(hOut, &r, sizeof(RECORD), &bytesWritten, NULL);
		if (bytesWritten != sizeof(RECORD)) {
			_ftprintf(stderr, _T("Line read: %s %s %s %s\n"),
				r.ip, r.user, r.date, r.duration);
			fclose(fpIn);
			CloseHandle(hOut);
			return 4;
		}
	}

	_tprintf(_T("File %s correctly written\n"), argv[2]);
	fclose(fpIn);
	CloseHandle(hOut);

	HANDLE hIn = CreateFile(argv[2], GENERIC_READ, 0, NULL, OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL, NULL);
	if (hIn == INVALID_HANDLE_VALUE) {
		_ftprintf(stderr, _T("Cannot open output file %s. Error: %x\n"),
			argv[2], GetLastError());
		return 5;
	}

	_tprintf(_T("Reading file %s to check errors\n"), argv[2]);
	DWORD bytesRead;
	while (ReadFile(hIn, &r, sizeof(RECORD), &bytesRead, NULL) && bytesRead > 0) {
		_tprintf(_T("Line read: %s %s %s %s\n"),
			r.ip, r.user, r.date, r.duration);
	}

	CloseHandle(hIn);
	Sleep(5000);
	return 0;
}