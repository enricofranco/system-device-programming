# Exercise 1

The solution of *exercise 1* requires three semaphores, following a **simple synchronization** pattern.

- `me` to guarantee mutex among threads which share a global variable;
- `readData` to awake the server when a client read a data;
- `writtenData` to awake the correspondant client when the server elaborated a proper response.

When a client thread terminates, it raises a signal, therefore the proper routine is activated: here, it is checked the number of threads already terminated and when the last one arrives, the server is signalled on `readData`. If this trick is not performed, the server will remain blocked forever, given that no client would signal `readData` on its termination.
