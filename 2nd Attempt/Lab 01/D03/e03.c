#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <ctype.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <pthread.h>

#define	ARGUMENT_ERROR	1
#define	FILE_ERROR		2

#define MAX_SLEEP		3

#define MAIN	"[MAIN]  "
#define PROC	"[PROC]  "
#define OUT		"[OUT]   "

/* Global variables */
char next, this, last;

#ifdef DEBUG
	FILE *fpOut;
#endif

void* process(void* arg);
void* output(void* arg);

int main(int argc, char** argv) {
	pthread_t tidM, tidP, tidO;
	FILE* fpIn;

	if (argc < 2) {
		fprintf(stderr, "Usage: %s filename\n", argv[0]);
		exit(ARGUMENT_ERROR);
	}

	tidM = pthread_self();
	printf("%s%lu\t Started\n", MAIN, tidM);

	fpIn = fopen(argv[1], "r");
	if (fpIn == NULL) {
		fprintf(stderr, "Error opening file %s\n", argv[1]);
		exit(FILE_ERROR);
	}

#ifdef DEBUG
	fpOut = fopen("output.txt", "w");
	if (fpOut == NULL) {
		fprintf(stderr, "Error opening file %s\n", "output.txt");
		exit(FILE_ERROR);
	}
#endif

	next = fgetc(fpIn);
	printf("%s%lu\t Read character %c\n", MAIN, tidM, next);
	pthread_create(&tidP, NULL, process, NULL);
	pthread_join(tidP, NULL);

	while ((next = fgetc(fpIn)) != EOF) {
		last = this;

		printf("%s%lu\t Read character %c\n", MAIN, tidM, next);
		pthread_create(&tidP, NULL, process, NULL);
		pthread_create(&tidO, NULL, output, NULL);

		pthread_join(tidP, NULL);
		pthread_join(tidO, NULL);
	}
	fclose(fpIn);

	last = this;
	pthread_create(&tidO, NULL, output, NULL);
	pthread_join(tidO, NULL);

	exit(0);
}

void* process(void* arg) {
	pthread_t tid = pthread_self();

	this = toupper(next);
	printf("%s%lu\t Capitalized %c into %c\n", PROC, tid, next, this);
	pthread_exit(0);
}

void* output(void* arg) {
	pthread_t tid = pthread_self();\

	printf("%s%lu\t Found character %c\n", OUT, tid, last);
#ifdef DEBUG
	fprintf(fpOut, "%c", last);
#endif
	pthread_exit(0);
}
