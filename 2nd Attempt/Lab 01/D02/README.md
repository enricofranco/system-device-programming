# Exercise 2

The solution of *exercise 2* is straightforward. It simply extends the previous exercise with small enhancements and concurrency. In particular, it wrappes previous operations and it uses some system calls:

- `fork` to generate children;
- `wait` to wait children and get their return values.
