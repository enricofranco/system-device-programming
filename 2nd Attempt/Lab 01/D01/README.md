# Exercise 1

The solution of *exercise 1* is straightforward.

1. Generate random arrays;
2. Sort arrays;
3. Open files:
    - "Text" mode;
    - ANSI mode;
    - UNIX mode;
4. Write arrays on text files;
5. Write arrays on "binary" files:
    - ANSI mode;
    - UNIX mode.
