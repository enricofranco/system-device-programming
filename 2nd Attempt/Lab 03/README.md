# Exercise 1
Write a C program using Pthreads that tests the sem_trywait system call on a *Producer & Consumer* problem using two buffers.

In particular, the main thread must create a Producer and a Consumer thread, and must wait for their termination.

The Producer thread:

- Loops 10000 times sleeping a random number of milliseconds (1-10);
- Fills a variable `ms` with the current time in milliseconds, using the function
```
long long current_timestamp() {
	struct timeval te;
	gettimeofday(&te, NULL); // get current time
	long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000;
	return milliseconds;
}
```
- Selects randomly a buffer `urgent` or `normal` in which it will put the value of ms. It must select the normal buffer 80% of the times;
- Prints the message: "putting <ms> in buffer <buffer>", where <ms> is the value, and <buffer> is either urgent or normal;
- Puts the value of `ms` in the selected buffer;
- Signals on a semaphore that something has been produced.

The Consumer thread loops:

- Sleeps 10 milliseconds;
- Waiting that something has been produced;
- Tries to get an ms from the urgent buffer, but if it is empty it can proceed to get it from the normal buffer;
- Prints the value of `ms` and the buffer identity (`urgent` or `normal`) from which it has got this
value.

# Exercise 2
Write a C program using Pthreads that implements the *Producer & Consumer* protocol, where

- Producer thread produces and puts on a shared buffer 10000 integer numbers (from 0 to 9999);
- Consumer thread gets and prints the received numbers.

You must solve the problem implementing the semaphore functions: `sema_init`, `sema_wait`, `sema_post` using **conditions**. You cannot use standard semaphores.

# Exercise 3
Write a C program using Pthreads that sorts the content of a binary file including a sequence of integer numbers. Implement a threaded quicksort program where the recursive calls to quicksort are replaced by threads activations, i.e. sorting is done, in parallel, in different regions of the file. Use `mmap` to map the file as a vector in memory.

If the difference between the `right` and `left` indexes is less than a value `size`, given as an argument of the command line, sorting is performed by the standard *quicksort* algorithm.
