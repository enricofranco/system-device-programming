#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include <pthread.h>
#include <unistd.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define BUFFER_SIZE		16
#define LIMIT			100

/* Semaphore */
typedef struct sema_t {
	unsigned int counter;
	pthread_mutex_t *mutex;
	pthread_cond_t *condition;
} sema_t;

void sema_init(sema_t* c, unsigned int value) {
	c->counter = value;
	c->mutex = malloc(sizeof(pthread_mutex_t));
	c->condition = malloc(sizeof(pthread_cond_t));

	pthread_cond_init(c->condition, NULL);
	pthread_mutex_init(c->mutex, NULL);
}

void sema_wait(sema_t* c) {
	pthread_mutex_lock(c->mutex);
	while (c->counter == 0) {
		pthread_cond_wait(c->condition, c->mutex);
	}
	c->counter--;
	pthread_mutex_unlock(c->mutex);
}

void sema_post(sema_t* c) {
	pthread_mutex_lock(c->mutex);
	c->counter++;
	pthread_cond_signal(c->condition);
	pthread_mutex_unlock(c->mutex);
}

void sema_destroy(sema_t* c) {
	pthread_cond_destroy(c->condition);
	pthread_mutex_destroy(c->mutex);
	free(c);
}

sema_t* initCondition(unsigned int value) {
	sema_t* c = malloc(sizeof(sema_t));
	sema_init(c, value);
	return c;
}

void freeCondition(sema_t* c) {
	printf("Freeing condition...\n");
	sema_destroy(c);
	free(c);
}

/* Buffer */
typedef struct buffer_t {
	int *buffer;
	int size, in, out;
	sema_t *free, *full;
	char *name;
} buffer_t;

buffer_t* prepareBuffer(const int size, const char* name) {
	buffer_t* b = malloc(sizeof(*b));

	b->buffer = malloc(sizeof(b->buffer) * size);
	b->size = size;
	b->in = b->out = 0;
	b->name = strdup(name);
	b->free = initCondition(size);
	b->full = initCondition(0);

	return b;
}

void freeBuffer(buffer_t* b) {
	printf("Freeing buffer %s...\n", b->name);
	freeCondition(b->free);
	freeCondition(b->full);
	free(b->buffer);
	free(b->name);
	free(b);
}

/* Prototypes */
void* producer(void* param);
void* consumer(void* param);
void enqueue(buffer_t* b, int e);
int dequeue(buffer_t* b);

/* Global variables */
buffer_t *buffer;

int main(int argc, char** argv) {
	pthread_t tidP, tidC;

	setbuf(stdout, 0);
	srand(time(NULL));

	buffer = prepareBuffer(BUFFER_SIZE, "my_buffer");

	pthread_create(&tidP, NULL, producer, NULL);
	pthread_create(&tidC, NULL, consumer, NULL);

	pthread_join(tidP, NULL);
	pthread_join(tidC, NULL);

	freeBuffer(buffer);

	exit(0);
}

void* producer(void* param) {
	int i, waitTime;

	for (i = 0; i < LIMIT; i++) {
		waitTime = rand() % 10;
		usleep(waitTime * 1000);
		enqueue(buffer, i);
	}
	pthread_exit(0);
}

void* consumer(void* param) {
	int i;

	for (i = 0; i < LIMIT; i++) {
		usleep(10 * 1000);
		dequeue(buffer);
	}
	pthread_exit(0);
}

void enqueue(buffer_t* b, int e) {
	printf("putting %02d in buffer %s in position %02d\n", e, b->name, b->in);
	sema_wait(b->free);
	b->buffer[b->in] = e;
	b->in = (b->in + 1) % b->size;
	sema_post(b->full);
}

int dequeue(buffer_t* b) {
	int e;

	sema_wait(b->full);
	printf("getting an element from buffer %s in position %02d", b->name, b->out);
	e = b->buffer[b->out];
	b->out = (b->out + 1) % b->size;
	sema_post(b->free);
	printf(": %02d\n", e);

	return e;
}
