
# Exercise 4

After the allocation of `v1`, `v2` and `mat`, the main program creates `k` threads, passing to them:

- `k`, representing the size of the arrays `v1`, `v2` and the square matrix `mat`;
- pointers to `v1`, `v2` and `mat`;
- pointer `intermediateArray`, pointing to an intermediate array in which store the intermediate product `mat * v2`;
- `index`, indicating on which of the matrix and intermediate array the thread has to work.

And waits for their termination. Then, it simply print the result;

Each thread performs the scalar product between the `index`-th row of the matrix `mat` and the array `v2`, stores the result in the array `intermediateArray` in position `index` and, in mutual exclusion, increments a global counter in order to exploit a synchronization barrier paradigm. In this way, the last thread is able to recognize such a situation and it executes the scalar product between `v1` and the intermediate array, producing the desired result in a global variable seen by the main program.
